# Modelo de projeto de hardware em kicad 8

Esse modelo já está parametrizado com as capabilidades básicas de projeto e já contém uma lista de netclasses definidas.

# Variáveis de ambiente
Esse template utiliza um fork da biblioteca padrão kicad, para que você possa utilizar o modelo adequadamente algumas variáveis de ambiente devem estar configuradas na sua instalação do kicad


KICAD8_3DMODEL_DIR	${KICAD_LIB_BY_JULIO}\kicad-packages3D	
KICAD8_3RD_PARTY	C:\Users\jcrad\OneDrive\Documentos\KiCad\8.0\3rdparty	
KICAD8_FOOTPRINT_DIR	${KICAD_LIB_BY_JULIO}\kicad-footprints	
KICAD8_SYMBOL_DIR	${KICAD_LIB_BY_JULIO}\kicad-symbols	
KICAD8_TEMPLATE_DIR	${KICAD_LIB_BY_JULIO}\kicad-templates	
KICAD_LIB_BY_JULIO	<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< Insira aqui o path da biblioteca kicad-lib_byJulio
KICAD_USER_TEMPLATE_DIR	${KICAD_LIB_BY_JULIO}\kicad-templates\Projects\JULIO	


# Critérios para regras elétricas

## largura de trilha
A largura é definida em função da corrente circulante pela trilha considerando os seguintes parâmetros. Além disso a largura também é limitada pela capabilidade mínima de processo
* Espessura da trilha = 35 um
* Aumento de temperatura permitido = 10 °C

## Espaçamento elétrico
O espaçamento é definido a partir da diferênça de tensão máxima, da elevação do local. Além disso a distância também é limitada pela capabilidade mínima de processo
* Elevação máxima = 3050 m
* Isolação = condutores externos, sem revestimentos


## Alguns links de interesse
* [Linha de comando no kicad] (https://docs.kicad.org/master/en/cli/cli.html)
* [Linguagem MD - readme] (https://raullesteves.medium.com/github-como-fazer-um-readme-md-bonit%C3%A3o-c85c8f154f8)
* [Automação de comandos GIT] (https://gitbetter.substack.com/p/automate-repetitive-tasks-with-custom)


## Recursos implementados
- [X] Lista básica de netlists
- [X] Padrão das folhas de trabalho
- [X] Regras de desenho nas capabilidades padrão
- [X] Script de geração de BOM
- [X] Adicionar regras de netclasses para PCB
- [X] Script de geração de documentação de esquema elétrico
- [X] Script de geração de documentação de PCI
- [ ] Script de geração de prints automáticos
